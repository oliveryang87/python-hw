#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 31 21:08:18 2017

@author: BabeLu
"""


# First, import numpy
import numpy as np

# Second, construct a 5 by 5 random matrix, and normalize each row to get a transition matrix
P = np.random.rand(5, 5) 
# nomarlize
for i in range(P.shape[0]):
    P[i,:]=P[i,:]/P.sum(1)[i]
# Third, start from a random normalized probability distribution p_0 and take 50 steps to get p_50
p_0 = np.random.rand(5,1) 
temp = p_0
for step in range(50):
    temp = np.dot(P.T,temp)
p_50 = temp
print(p_50)
# Fourth, normalize the eigenvector and use the eigenvector of P.T. with eigenvalue 1 to obtain p_s
d,v=np.linalg.eig(P.T)
p_s = (v[:,0]/v[:,0].sum()).reshape(5,1)
# Last, check if p_50 and p_s are equal to tolerance le-5
print(p_s)
if (p_50-p_s).sum() > 1e-5:
    print('p_50 and p_stationary are not equal')
else:
    print('p_50 and p_stationary are equal')
    